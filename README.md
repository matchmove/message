# message
--
    import "bitbucket.org/matchmove/message"


## Usage

#### type Message

```go
type Message struct {
	Prefix        string
	HeaderVersion int
	PayloadSize   int
	Action        string //byte representation of the Message name
	Version       string
	Payload       string
}
```

Message ....

#### func  New

```go
func New() (m *Message)
```
New message

#### func (*Message) Parse

```go
func (me *Message) Parse(conn net.Conn) (err error)
```
Parse serialize data

#### func (*Message) Serialize

```go
func (me *Message) Serialize(HeaderVer int, MsgName string, MsgVer string, tPayload string) (response []byte)
```
Serialize ...
