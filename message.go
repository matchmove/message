package message

import (
	"fmt"
	"net"
	"strconv"
	"strings"
)

// Message ....
type Message struct {
	Prefix        string
	HeaderVersion int
	PayloadSize   int
	Action        string //byte representation of the Message name
	Version       string
	Payload       string
}

// New message
func New() (m *Message) {
	m = &Message{}
	return
}

// Serialize ...
func (me *Message) Serialize(HeaderVer int, MsgName string, MsgVer string, tPayload string) (response []byte) {

	if HeaderVer > 9 {
		HeaderVer = 0
	}

	response = []byte(fmt.Sprintf("MxM%1d%010d%-14s%-7s:%s", HeaderVer, len(tPayload), MsgName, MsgVer, tPayload))

	return
}

// Parse serialize data
func (me *Message) Parse(conn net.Conn) (err error) {
	var (
		buffer                                         = make([]byte, 37)
		iHeaderBytesRead, iPayloadBytesRead, iByteRead int
	)

	//MxM31234567890TestingStructx1.2.3.1:
	//|  | |        |             |      \-the last byte is : to indicate the end of the structure
	//|  | |        |             \-version 7 bytes
	//|  | |        \-struct name 14 bytes
	//|  | \-message size 10 bytes
	//|  \-header version 1 byte
	//\- Magic Str
	//later version{60,"test","1.2.2"}{"account",100,"USD"}
	//MxM30000000018TestingStructx1.2.3.1:{"test",100,"xxx"}

	for ok := true; ok; ok = (iHeaderBytesRead < 36) {
		if iByteRead, err = conn.Read(buffer[iHeaderBytesRead:]); err != nil {
			fmt.Println("Error reading:", err.Error())
			return
		}

		iHeaderBytesRead += iByteRead
	}

	me.Prefix = string(buffer[:3])

	if me.HeaderVersion, err = strconv.Atoi(string(buffer[3:4])); err != nil {
		return
	}

	if me.PayloadSize, err = strconv.Atoi(string(buffer[4:14])); err != nil {
		return
	}
	me.Action = strings.Trim(string(buffer[14:28]), " ")
	me.Version = string(buffer[28:36])

	buffer = make([]byte, me.PayloadSize)
	for ok := true; ok; ok = (iPayloadBytesRead < me.PayloadSize) {
		if iByteRead, err = conn.Read(buffer[iPayloadBytesRead:]); err != nil {
			fmt.Println("Error reading:", err.Error())
		}
		iPayloadBytesRead += iByteRead
	}
	me.Payload = string(buffer)

	return
}
